var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

// var podatkiSpomin = ["admin/nimda", "gost/gost"];

/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
*/
// res.send({status: "status", napaka: "Opis napake"});

app.get('/api/prijava', function(req, res) {
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo);

	var napaka = "";
	if (uporabniskoIme == null || uporabniskoIme.length == 0 || geslo == null || geslo.length == 0)
		napaka = "Napačna zahteva.";
	else if (!uspesno) {
		napaka = "Avtentikacija ni uspela.";
	}

	res.send({status: uspesno, napaka: napaka});
});

/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
*/

app.get('/prijava', function(req, res) {
	
//res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>Krneki</b> nima pravice prijave v sistem!</p></body></html>");

	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo);

	var odgovor = "<html><head><title>" + (uspesno == true ? "Uspešno" : "Napaka") + "</title><style>body {padding:10px}</style></head>";
	odgovor += "<body>";
	if (uspesno)
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> uspešno prijavljen v sistem!</p>";
	else
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> nima pravice prijave v sistem!</p>";

	odgovor += "</body></html>";
	res.send(odgovor);
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];
var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync(__dirname + "/public/podatki/" + "uporabniki_streznik.json").toString());


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
*/
// var podatkiDatotekaStreznik = {};

/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
*/

function preveriSpomin(uporabniskoIme, geslo) {
	for (i in podatkiSpomin) {
		username = podatkiSpomin[i].split("/")[0];
		password = podatkiSpomin[i].split("/")[1];
		if (uporabniskoIme == username && geslo == password)
			return true;
	}
	return false;
}

/**
* TODO: Potrebna je implementacija tega dela funkcionalnosti
*/

function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for (i in podatkiDatotekaStreznik) {
		username = podatkiDatotekaStreznik[i]["uporabnik"];
		password = podatkiDatotekaStreznik[i]["geslo"];
		if (uporabniskoIme == username && geslo == password)
			return true;
	}
	return false;
}